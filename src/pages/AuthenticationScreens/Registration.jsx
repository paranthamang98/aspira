import React, { useEffect } from "react";
import { Controller, useForm } from "react-hook-form";
import FormInput from "../../components/input-form/FormInput";
import "./AuthenticationLayout/LoginPage.scss";
import Button from "../../components/button/Button";
import { useDispatch, useSelector } from "react-redux";
import {
  Register,
  TechnologiesList,
} from "../../redux/actions/loginAuthActions";

function Registration() {
  const dispatch = useDispatch();
  const TechnologiesLists = useSelector(
    ({ loginAuth }) => loginAuth?.TechnologiesList?.data
  );

  console.log(TechnologiesLists);
  useEffect(() => {
    dispatch(TechnologiesList());
  }, []);
  const form = useForm({
    defaultValues: "",
    mode: "all",
  });

  useEffect(() => {
    form.reset();
  }, [form]);
  const { register, control, handleSubmit, formState, setValue, watch } = form;
  const { errors } = formState;

  const Submit = (data) => {
    console.log(data);
    dispatch(Register(data));
  };
  return (
    <div className="registration">
      <form className="registrationForm" onSubmit={handleSubmit(Submit)}>
        <h2>Registration Form</h2>
        <div className="registrationForm_inner">
          <FormInput
            label={"First Name"}
            placeholder={"Enter First Name"}
            // type={"date"}
            errorMSG={errors.firstName?.message}
            required={{
              ...register("firstName", {
                required: "First Name  is required",
              }),
            }}
          />
          <FormInput
            label={"Last Name"}
            placeholder={"Enter Last Name"}
            // type={"date"}
            errorMSG={errors.lastName?.message}
            required={{
              ...register("lastName", {
                required: "Last Name  is required",
              }),
            }}
          />

          <FormInput
            label={"Date of Birth"}
            placeholder={"Enter Date of Birth"}
            type={"date"}
            errorMSG={errors.dob?.message}
            required={{
              ...register("dob", {
                required: "Date of Birth  is required",
              }),
            }}
          />
          <FormInput
            label={"Email"}
            placeholder={"Email"}
            errorMSG={errors.email?.message}
            required={{
              ...register("email", {
                required: "Email is required",
                validate: {
                  matchPattern: (v) =>
                    /^\w+([.-]?\w+)*@\w+([.-]?\w+)*(\.\w{2,3})+$/.test(v) ||
                    "Email address must be a valid address",
                },
              }),
            }}
          />

          <FormInput
            type="number"
            label={"Mobile Number"}
            placeholder={"Mobile Number"}
            errorMSG={errors.mobile_no?.message}
            required={{
              ...register("mobile_no", {
                required: "password is required",
                validate: {
                  matchPattern: (v) =>
                    /^([+]\d{2}[ ])?\d{10}$/.test(v) ||
                    "please enter valid mobile number",
                },
                // validate: {
                //     minLength: (v) =>
                //     v.length >= 11 || "please enter valid mobile number",
                //     maxLength: (v) =>
                //     v.length <= 9 || "please enter valid mobile number",
                // },
              }),
            }}
          />

          <div className="radio_button">
            <FormInput
              radioLabel={"Gender"}
              label={"Male"}
              // placeholder={"Enter Degree"}
              value={"male"}
              type={"radio"}
              errorMSG={errors.gender?.message}
              required={{
                ...register("gender", {
                  required: "Gender is required",
                }),
              }}
            />
            <FormInput
              label={"Female"}
              // placeholder={"Enter Degree"}
              value={"female"}
              type={"radio"}
              // errorMSG={errors.gender?.message}
              required={{
                ...register("gender", {
                  required: "Gender is required",
                }),
              }}
            />
          </div>

          <FormInput
            label={"joing data"}
            placeholder={"Enter Date of Birth"}
            type={"date"}
            errorMSG={errors.join_date?.message}
            required={{
              ...register("join_date", {
                required: "Date of Birth  is required",
              }),
            }}
          />

          <FormInput
            label={"Father’s name"}
            placeholder={"Enter Father’s name"}
            type={"text"}
            errorMSG={errors.parent_name?.message}
            required={{
              ...register("parent_name", {
                required: "Date of Birth  is required",
              }),
            }}
          />
          <FormInput
            label={"Father’s Mobile number"}
            placeholder={"Enter Father’s Mobile number"}
            type={"number"}
            errorMSG={errors.parent_mobile_no?.message}
            required={{
              ...register("parent_mobile_no", {
                required: "Father’s Mobile number  is required",
                validate: {
                  matchPattern: (v) =>
                    /^([+]\d{2}[ ])?\d{10}$/.test(v) ||
                    "please enter valid mobile number",
                },
              }),
            }}
          />
          <FormInput
            label={"Last graduation"}
            placeholder={"Enter Last graduation"}
            type={"text"}
            errorMSG={errors.graduation_details?.message}
            required={{
              ...register("graduation_details", {
                required: "Last graduation is required",
              }),
            }}
          />
          <FormInput
            label={"Technology"}
            placeholder={"Enter Technology"}
            select={true}
            errorMSG={errors.preferd_techId?.message}
            options={TechnologiesLists}
            required={{
              ...register("preferd_techId", {
                required: "Technology  is required",
              }),
            }}
          />
          <FormInput
            label={"Linkedin"}
            placeholder={"Enter Linkedin"}
            type={"text"}
            errorMSG={errors.linkedIn_id?.message}
            required={{
              ...register("linkedIn_id", {
                required: "Linkedin  is required",
              }),
            }}
          />

          <Controller
            name="resume"
            control={control}
            defaultValue={null}
            render={({ field }) => (
              <input
                className="file-input"
                type="file"
                // name="certificates"
                accept=".pdf"
                // ref={fileInputRef}
                onChange={(e) => {
                  const file = e.target.files[0];
                  field.onChange(file);
                  setValue("resume", file);
                  console.log("ff", file);
                }}
              />

              //         <FormInput
              //   label={"Linkedin"}
              //   placeholder={"Enter Linkedin"}
              //   type={"text"}
              //   function={}
              //   errorMSG={errors.linkedIn_id?.message}
              //   required={{
              //     ...register("linkedIn_id", {
              //       required: "Linkedin  is required",
              //     }),
              //   }}
              // />
            )}
          />
        </div>
        <div className="radio_section">
          <div className="radio_button">
            <FormInput
              radioLabel={"Mode"}
              label={"On-site"}
              // placeholder={"Enter Degree"}
              value={"1"}
              type={"radio"}
              errorMSG={errors.mode?.message}
              required={{
                ...register("mode", {
                  required: "Gender is required",
                }),
              }}
            />
            <FormInput
              label={"Remote"}
              // placeholder={"Enter Degree"}
              type={"radio"}
              value={"2"}
              // errorMSG={errors.mode?.message}
              required={{
                ...register("mode", {
                  required: "Gender is required",
                }),
              }}
            />
          </div>
          <div className="radio_button">
            <FormInput
              radioLabel={"Session"}
              label={"Full time"}
              // placeholder={"Enter Degree"}
              value={"1"}
              type={"radio"}
              errorMSG={errors.availability?.message}
              required={{
                ...register("availability", {
                  required: "Gender is required",
                }),
              }}
            />
            <FormInput
              label={"Part time"}
              // placeholder={"Enter Degree"}
              value={"2"}
              type={"radio"}
              // errorMSG={errors.availability?.message}
              required={{
                ...register("availability", {
                  required: "Gender is required",
                }),
              }}
            />
          </div>
          <div className="radio_button">
            <FormInput
              radioLabel={"Currently Working"}
              label={"Yes"}
              value={"1"}
              // placeholder={"Enter Degree"}
              type={"radio"}
              errorMSG={errors.working_status?.message}
              required={{
                ...register("working_status", {
                  required: "Gender is required",
                }),
              }}
            />
            <FormInput
              label={"No"}
              value={"0"}
              // placeholder={"Enter Degree"}
              type={"radio"}
              // errorMSG={errors.working_status?.message}
              required={{
                ...register("working_status", {
                  required: "Gender is required",
                }),
              }}
            />
          </div>
          <div className="radio_button">
            <FormInput
              radioLabel={"System Facility"}
              label={"Yes"}
              // placeholder={"Enter Degree"}
              type={"radio"}
              value={"1"}
              errorMSG={errors.laptop_status?.message}
              required={{
                ...register("laptop_status", {
                  required: "Gender is required",
                }),
              }}
            />
            <FormInput
              label={"No"}
              // placeholder={"Enter Degree"}
              type={"radio"}
              value={"0"}
              // errorMSG={errors.laptop_status?.message}
              required={{
                ...register("laptop_status", {
                  required: "Gender is required",
                }),
              }}
            />
          </div>
        </div>

        <div className="radio_button">
          <Button>Cancel</Button>
          <Button type="submit">Submit</Button>
        </div>
      </form>
    </div>
  );
}

export default Registration;
