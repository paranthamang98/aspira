import React, { useEffect, useState } from "react";
import { Layout, TabText } from "../../components/Layout/Layout";
import "./MyProgress.scss";
import ProfileTable from "../../components/ProfileTable/ProfileTable";
import ProfileLayout from "../../components/Profilelayout/ProfileLayout";
import { EducationIcon, closeIcon, searchIcon } from "../../utils/images";
import { useForm } from "react-hook-form";
import FormInput from "../../components/input-form/FormInput";
import Button from "../../components/button/Button";
import { SubTitele } from "../../components/subTitele/SubTitele";
import { useDispatch, useSelector } from "react-redux";
import {
  TimeSheetList,
  TimeSheetListCreate,
  TimeSheetListDelete,
} from "../../redux/actions/TimeSheetAction";
import { courseList } from "../../redux/actions/courseAction";
import { ProductiveRateList } from "../../redux/actions/ProductiveRateAction";
import Popup from "../../components/popup/Popup";

export default function MyProgress() {
  const dispatch = useDispatch();

  const [activeTab, setActiveTab] = useState(false);
  const [isModalOpen, setIsModalOpen] = useState(false);
  const [createNew, setCreateNew] = useState(false);
  const [editValue, setEditValue] = useState({});
  const [radioValues, setRadioValues] = useState(editValue.type);
  console.log("values", editValue.type, radioValues);

  const ActiveTab = (data) => {
    // console.log(data);
    setActiveTab(data);
  };

  useEffect(() => {
    dispatch(TimeSheetList());
    dispatch(courseList());
  }, []);

  useEffect(() => {
    dispatch(ProductiveRateList());
  }, []);

  const timeSheetList = useSelector(
    ({ timeSheetList }) => timeSheetList?.timeSheetList
  );

  const productiveRate = useSelector(
    ({ productiveRate }) => productiveRate?.productiveRateList
  );

  const technologyOption = useSelector(({ courseList }) => courseList.courses);
  console.log("pr", timeSheetList);

  const productiveValue =
    productiveRate.find((item) => item.type === "Productivity")?.value || 0;

  const [timeSheetData, setTimeSheetData] = useState(timeSheetList);

  const {
    register,
    control,
    handleSubmit,
    formState: { errors },
    setValue,
    watch,
    reset,
  } = useForm({
    mode: "all",
  });

  const {
    register: register2,
    formState: { errors: errors2 },
    handleSubmit: handleSubmit2,
    reset: reset2,
  } = useForm({
    mode: "all",
  });

  const {
    register: register3,
    formState: { errors: errors3 },
    handleSubmit: handleSubmit3,
    setValue: setValue3,
  } = useForm({
    defaultValues: editValue,
    mode: "all",
  });

  useEffect(() => {
    Object.keys(editValue).forEach((key) => {
      setValue3(key, editValue[key]);
    });
    // setRadioValues(editValue.type)
  }, [editValue, setValue3]);

  // useEffect(() => {

  // }, [])
  const getBorderColor = () => {
    if (productiveValue < 25) {
      return {
        borderColor: "red",
        quote: "Stay focused and keep pushing forward!",
      };
    } else if (productiveValue >= 25 && productiveValue < 60) {
      return {
        borderColor: "blue",
        quote: "Progress comes from taking one step at a time.",
      };
    } else if (productiveValue >= 60 && productiveValue < 90) {
      return {
        borderColor: "yellow",
        quote: "You're doing well; keep up the good work!",
      };
    } else {
      return {
        borderColor: "green",
        quote: "Your dedication is truly inspiring.",
      };
    }
  };

  const deleteTimeSheet = (item) => {
    dispatch(TimeSheetListDelete(item));
  };

  // const handleReset = () => {
  //   reset();
  //   reset2();
  //   setTimeSheetData(timeSheetList);
  // };
  // let filteredData;
  // let filteredDatas;
  // const handleFilterByDate = (data) => {
  //   const fromDate = new Date(data.fromDate);
  //   const toDate = new Date(data.toDate);
  //   filteredData = timeSheetList.filter((item) => {
  //     const currentDate = new Date(item.date);
  //     return currentDate >= fromDate && currentDate <= toDate;
  //   });
  //   // setTimeSheetData(filteredData);
  // };

 
  let filterData;
  let filterDatas;

  const handleReset = () => {
    reset();
    reset2();
    setTimeSheetData(timeSheetList);
  };

  const handleFilterByDate = (data) => {
    const fromDate = new Date(data.fromDate);
    const toDate = new Date(data.toDate);
    const filteredData = timeSheetList.filter((item) => {
      const currentDate = new Date(item.date);
      return currentDate >= fromDate && currentDate <= toDate;
    });
    setTimeSheetData(filteredData);
  };

   const handleFilterByMultiple = (data) => {
     const month = new Date(data.month).getMonth() + 1;
     const hours = parseInt(data.hours);
     const category = data.category;

     const categoryValues = {
       1: "Productive Effort",
       2: "Leave",
       3: "System/Power issue",
     };
     const filteredDatas = timeSheetList.filter((item) => {
       const dataDate = new Date(item.date).getMonth() + 1;
       const monthMatches = month === dataDate;

       const startTime = new Date(`1970-01-01T${item.start}`);
       const endTime = new Date(`1970-01-01T${item.end}`);
       const hoursDifference = Math.abs(
         (startTime - endTime) / (1000 * 60 * 60)
       );

       const hourMatch = hours === hoursDifference;
       const categoryMatches = categoryValues[category] === item.type;
       console.log(monthMatches, hourMatch, categoryMatches, category, item.type);

       return monthMatches && hourMatch && categoryMatches;
     });
     setTimeSheetData(filteredDatas);
   };
  // const handleFilterByMultiple = (data) => {
  //   const month = new Date(data.month).getMonth() + 1;
  //   const hours = parseInt(data.hours);
  //   const category = data.category;

  //   const filteringData = timeSheetList.filter((item) => {
  //     const dataDate = new Date(item.date).getMonth() + 1;
  //     const monthMatches = month === dataDate;

  //     const startTime = new Date(`1970-01-01T${item.start}`);
  //     const endTime = new Date(`1970-01-01T${item.end}`);
  //     const hoursDifference = Math.abs(
  //       (startTime - endTime) / (1000 * 60 * 60)
  //     );

  //     const hourMatch = hours === hoursDifference;

  //     const categoryMatches = category === item.type;

  //     return monthMatches && hourMatch && categoryMatches;
  //   });
  //   setTimeSheetData(filteringData);
  //   console.log(data, filteringData);
  // };

  useEffect(() => {
    setTimeSheetData(timeSheetList);
  }, [timeSheetList]);

  // useEffect(() => {
  //   // setTimeSheetData(filteredDatas);
  // }, [handleFilterByMultiple]);

  // useEffect(() => {
  //   // setTimeSheetData(filteredData);
  // }, [handleFilterByDate]);

  const addTimeSheet = (data) => {
    console.log("in progress before dispatching", data);
    createNew
      ? dispatch(TimeSheetListCreate(data))
      : console.log("data on edit", data);
    setIsModalOpen(false);
    setCreateNew(false);
    // console.log('data of pop-up', data)
  };

  const newTimeSheet = () => {
    setIsModalOpen(true);
    setEditValue({
      date: "",
      start: "",
      end: "",
      type: "",
      links: "",
      description: "",
      technology_id: "",
    });
    setCreateNew(true);
  };

  function closePopup() {
    setIsModalOpen(false);
    setCreateNew(false);
  }

  function editFunction(item) {
    console.log("item of edit", item);
    setEditValue(item);
    console.log("edit values", editValue);
    setIsModalOpen(true);
  }

  const tabelHeaders = [
    {
      id: 1,
      tabelHeader: "#",
      datavalue: "#",
    },
    {
      id: 2,
      tabelHeader: "Date",
      datavalue: "date",
    },
    {
      id: 3,
      tabelHeader: "Activity",
      datavalue: "type",
    },
    {
      id: 4,
      tabelHeader: "Description",
      datavalue: "description",
    },
    {
      id: 5,
      tabelHeader: "Hours",
      datavalue: "hours",
    },
    {
      id: 5,
      tabelHeader: "Link",
      datavalue: "links",
    },
    {
      id: 9,
      tabelHeader: "Actions",
      datavalue: "actions",
    },
  ];

  const optionsList = [
    {
      id: 1,
      value: "Productive Effort",
      name: "Productive Effort",
    },
    {
      id: 2,
      value: "Leave",
      name: "Leave",
    },
    {
      id: 3,
      value: "System/Power issue",
      name: "System/Power Issue",
    },
  ];

  const productiveTableHeaders = [
    {
      id: 1,
      tabelHeader: "Activity Category",
      datavalue: "type",
    },
    {
      id: 2,
      tabelHeader: "Hours",
      datavalue: "hourData",
    },
    {
      id: 3,
      tabelHeader: "Days",
      datavalue: "days",
    },
  ];

  // const technologyList = [
  //   {

  //   }
  // ]

  const type = [
    { value: "1", label: "Productive Effort" },
    { value: "2", label: "System/Power Issue" },
    { value: "3", label: "Leave" }
  ];

  return (
    <>
      <Layout header={"My Progress"}>
        <div className="tad_header">
          <TabText
            classActive={true}
            states={activeTab}
            text={"Achieved"}
            activeFunction={() => ActiveTab(true)}
          />
          <TabText
            classActive={false}
            states={activeTab}
            text={"Request Interview"}
            activeFunction={() => ActiveTab(false)}
          />
        </div>
        <>
          {!activeTab ? (
            <>
              <div className="quote_box">
                <div className="border_right" style={getBorderColor()}></div>
                <h4>{getBorderColor().quote}</h4>
              </div>
              <div className="profile-section">
                <SubTitele>Journey Timeline</SubTitele>
                <div className="time">
                  {[1, 2, 3, 4].map((index) => {
                    return (
                      <div className="time_stage" key={index}>
                        <p className="time_stage_no">Stage 01</p>

                        <h5 className="time_stage_date">
                          <span>12 AUG 2023</span>
                        </h5>

                        <p className="time_stage_start">Web Dev start</p>
                      </div>
                    );
                  })}
                </div>
              </div>
              <div className="profile-section time_table">
                <ProfileTable
                  tabelHeaders={productiveTableHeaders}
                  values={productiveRate}
                  subTitele={"Productive rate"}
                />
              </div>
            </>
          ) : (
            <>
              {timeSheetList && timeSheetList !== 0 ? (
                <div className="profile-section">
                  <div className="form_section">
                    <form
                      key={1}
                      className="form_field"
                      onSubmit={handleSubmit(handleFilterByDate)}>
                      <FormInput
                        label={"From"}
                        // placeholder={"Enter College Name"}
                        type={"date"}
                        errorMSG={errors.fromDate?.message}
                        required={{
                          ...register("fromDate", {
                            required: "fromDate is required",
                          }),
                        }}
                        // value={watch("college_name")}
                      />
                      <FormInput
                        label={"To"}
                        type={"date"}
                        // placeholder={"Enter College Name"}
                        errorMSG={errors.toDate?.message}
                        required={{
                          ...register("toDate", {
                            required: "To Date is required",
                          }),
                        }}
                        // value={watch("college_name")}
                      />
                      <Button type={"submit"}>
                        <img src={searchIcon} alt="" />
                      </Button>
                    </form>

                    <form
                      key={2}
                      className="form_field"
                      onSubmit={handleSubmit2(handleFilterByMultiple)}>
                      <FormInput
                        label={"Month"}
                        type={"date"}
                        // placeholder={"Enter College Name"}
                        // select={true}
                        errorMSG={errors2.month?.message}
                        required={{
                          ...register2("month", {
                            required: "month is required",
                          }),
                        }}
                        // value={watch("college_name")}
                      />
                      <FormInput
                        label={"Hours"}
                        type={"time"}
                        // placeholder={"Enter College Name"}
                        errorMSG={errors2.hours?.message}
                        required={{
                          ...register2("hours", {
                            required: "Hours is required",
                          }),
                        }}
                        // value={watch("college_name")}
                      />
                      <FormInput
                        label={"Category"}
                        select={true}
                        options={optionsList}
                        // placeholder={"Enter College Name"}
                        errorMSG={errors2.category?.message}
                        required={{
                          ...register2("category", {
                            required: "Category is required",
                          }),
                        }}
                        // value={watch("college_name")}
                      />
                      <Button type={"submit"}>
                        <img src={searchIcon} alt="" />
                      </Button>
                      <Button functions={handleReset}> Reset</Button>
                    </form>
                  </div>
                  <ProfileTable
                    tabelHeaders={tabelHeaders}
                    values={timeSheetData}
                    subTitele={"Timesheet"}
                    openPopup={newTimeSheet}
                    openEditPopup={editFunction}
                    deleteItem={deleteTimeSheet}
                  />
                </div>
              ) : (
                <ProfileLayout
                  content={"No Timesheet records found"}
                  img={EducationIcon}
                  buttonContent={"+ Add Time sheet"}
                  // openPopup={newUser}
                />
              )}
            </>
          )}
          <Popup
            isOpen={isModalOpen}
            onRequestClose={() => setIsModalOpen(false)}>
            

            <div className="popup_header">
              <h3>Add TimeSheet</h3>
              <img onClick={closePopup} src={closeIcon} alt="" />
            </div>

            <form onSubmit={handleSubmit3(addTimeSheet)}>
              <div className="form_section add">
                <FormInput
                  label={"Date"}
                  // placeholder={"Enter College Name"}
                  type={"date"}
                  errorMSG={errors3.date?.message}
                  required={{
                    ...register3("date", {
                      required: "date  is required",
                    }),
                  }}
                  // value={watch("college_name")}
                />
                <FormInput
                  label={"Start Time"}
                  // placeholder={"Enter Degree"}
                  type={"time"}
                  errorMSG={errors3.start?.message}
                  required={{
                    ...register3("start", {
                      required: "Satrt Time is required",
                    }),
                  }}
                />
                <FormInput
                  label={"End Time"}
                  // placeholder={"Enter Degree"}
                  type={"time"}
                  errorMSG={errors3.end?.message}
                  required={{
                    ...register3("end", {
                      required: "End Time is required",
                    }),
                  }}
                />
                <FormInput
                  label={"Technology"}
                  select={true}
                  options={technologyOption}
                  // placeholder={"Enter College Name"}
                  errorMSG={errors3.technology_id?.message}
                  required={{
                    ...register3("technology_id", {
                      required: "technology is required",
                    }),
                  }}
                  // value={watch("college_name")}
                />
             
                  

<FormInput
              radioLabel={"Activity"}
              type={"radio"}
              options={type}
              required={{
                ...register3("type"),
              }}
            />
               

                <FormInput
                  label={"Link (if any) :"}
                  placeholder={"Link of Practice part"}
                  errorMSG={errors3.links?.message}
                  clipboard={true}
                  required={{
                    ...register3("links", {
                      required: "Link is required",
                    }),
                  }}
                />
              </div>

              <FormInput
                label={"Description"}
                // placeholder={"Enter College Name"}
                // type={"text"}
                textarea={true}
                errorMSG={errors3.description?.message}
                required={{
                  ...register3("description", {
                    required: "description  is required",
                  }),
                }}
                // value={watch("college_name")}
              />

              <div className=" button_group">
              <Button type="button" functions={closePopup}>
                  Cancel
                </Button>
                <Button type="submit" functions={() => setIsModalOpen(true)}>
                  Add TimeSheet
                </Button>
              </div>
            </form>
          </Popup>
        </>
      </Layout>
    </>
  );
}
