import React, { useEffect, useState } from "react";
import { Layout } from "../../components/Layout/Layout";
import "./MyProjects.scss";
import FormInput from "../../components/input-form/FormInput";
import Button from "../../components/button/Button";
import { useDispatch, useSelector } from "react-redux";
import { projectsDetailList } from "../../redux/actions/projectDetailAction";
import { searchProjectIcon } from "../../utils/images";
import SCard from "../../components/smallCard/SCard";
import { useForm, Controller } from "react-hook-form";

export default function MyProjects() {
  const dispatch = useDispatch();
  let filteredData;

  useEffect(() => {
    dispatch(projectsDetailList());
  }, []);

  const projectList = useSelector(
    ({ projectDetailList }) => projectDetailList.projectDetailList
  );

  const [search, setSearch] = useState("");
  const [projectData, setProjectData] = useState(projectList);

  const handleGitHub = (gitHubLink) => {
    window.open(gitHubLink.repository_url, "_blank");
  };

  const handleView = (viewLink) => {
    window.open(viewLink.project_url, "_blank");
  };

  const handleChange = (e) => {
    setSearch(e.target.value);
    if (e.target.value === "") {
      setProjectData(projectList);
    }
  };

  const onFilter = () => {
    const inputValue = search.toLowerCase();

    if (projectList) {
      filteredData = projectList.filter((item) => {
        const projTitle = (
          item.project_name ||
          item.project_technology ||
          ""
        ).toLowerCase();

        return projTitle.includes(inputValue);
      });
      setProjectData(filteredData);
    }
  };

  useEffect(() => {
    if (search === "") {
      setProjectData(projectList);
    } else {
      onFilter();
    }
  }, [search, onFilter, projectList]);

  return (
    <>
      <Layout header={"My Projects"}>
        <div className="searchCont form-section">
          <input
            name="search"
            value={search}
            placeholder="Search by name"
            onChange={handleChange}
            className="textBox"
          />
          <Button className="btnSearch" function={onFilter}>
            <img src={searchProjectIcon} alt="searchIcon" />
          </Button>
        </div>
        <div className="grid_column">
          {projectData.map((item) => (
            <SCard
              type="project"
              description={item.description}
              image={item.image}
              name={item.project_name}
              duration={item.project_completed_duration}
              tech={item.project_technology}
              unique={item.project_id}
              handleLeftBtn={() => handleGitHub(item)}
              handleRightBtn={() => handleView(item)}
            />
          ))}
        </div>
      </Layout>
    </>
  );
}
