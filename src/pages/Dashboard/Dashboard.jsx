import React, { useEffect, useState } from "react";
import { Layout } from "../../components/Layout/Layout";
import "./Dashboard.scss";
import TimeBasedGreeting from "../../components/TimeBasedGreeting/TimeBasedGreeting";
import Courses from "../../components/courses/Courses";
import { AlertIcon, CoursesIcon, RewardsIcon } from "../../utils/images";
import { Cards } from "../../components/cards/Cards";
import { SeeAll, SubTitele } from "../../components/subTitele/SubTitele";
import YourChartComponent from "../../components/chat";
import Alert from "../../components/alert/Alert";
import { useDispatch, useSelector } from "react-redux";
import { useNavigate } from "react-router";
import { courseList } from "../../redux/actions/courseAction";
import { rewardsActionList } from "../../redux/actions/rewardActions";
import { Link } from "react-router-dom";


export default function Dashboard() {

  const dispatch = useDispatch();
  const navigate = useNavigate();
  const courses = useSelector((state) => state.courseList?.courses);

  useEffect(() => { 
    dispatch(courseList());
    dispatch(rewardsActionList());
  }, []);


  const handleSuccessfulLogin = () => {
    navigate("/my-learnings/course-detail");
  };


  const rewardData = useSelector(({ rewardLists }) => rewardLists?.rewards);

  const [alertIcon, setAlertIcon] = useState(false);

  return (
    <>
      <Layout header={"Dashboard"}>
        <div className="dashboard-inner">
          <div className="dashboard-left">
            <TimeBasedGreeting />
            <div className="course-info">
              <Link to={"/my-learnings"}>
              <Courses
                img={CoursesIcon}
                count={courses.length}
                color={"LightningYellow"}
                name={"My Courses"}
              />
              </Link>
              <Link to={"/my-learnings"}>
              <Courses
                img={CoursesIcon}
                count={courses.filter(courses => courses.status).length}
                color={"LawnParty"}
                name={"Completed Courses"}
              />
              </Link>
              <Link to={"/rewards"}>
              <Courses
                img={RewardsIcon}
                count={rewardData.length}
                color={"Pear"}
                name={"Certification & Rewards"}
              />
              </Link>
            </div>
            <div className="header-section">
              <SubTitele>My Courses</SubTitele>
              <SeeAll />
            </div>
            <div className="course-info">
              <Cards
                img={courses[0]?.image}
                stage={courses[0]?.name}
                title={"Stage - 1"}
                percent={courses[0]?.percentage}
              />
               <Cards
                img={courses[1]?.image}
                stage={courses[1]?.name}
                title={"Stage - 1"}
                percent={courses[1]?.percentage}
              />
            </div>
            <div className="course-info">
              <YourChartComponent
                chartId={"doughnut"}
                chartType={"doughnut"}
                header={"Productivity Overview"}
              />
              <YourChartComponent
                chartId={"bar"}
                chartType={"bar"}
                header={"Timesheet Overview"}
              />
            </div>
          </div>
          <div className={`dashboard-right ${alertIcon ? "active" : ""}`}>
            <Alert />
          </div>
          <div className="alert-icon" onClick={() => setAlertIcon(!alertIcon)}>
            <img src={AlertIcon} alt="" />
          </div>
        </div>
      </Layout>
    </>
  );
}
