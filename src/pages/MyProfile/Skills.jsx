import React, { useEffect, useState } from "react";
import { Layout } from "../../components/Layout/Layout";
import ProfileLayout from "../../components/Profilelayout/ProfileLayout";
import Button from "../../components/button/Button";
import { SubTitele } from "../../components/subTitele/SubTitele";
import { EducationIcon } from "../../utils/images";
import { skillsList, skillsUpdate } from "../../redux/actions/skillsAction";
import { useDispatch, useSelector } from "react-redux";

const Skills = () => {
  const dispatch = useDispatch();
  const skillsLis = useSelector((state) => state.skillsList);
  const [skill, setSkills] = useState({ skills: [] });
  const [create, setCreate] = useState(false);
  const [inputValue, setInputValue] = useState("");

  const handleChange = (event) => {
    const newValue = event.target.value;
    setInputValue(newValue);
  };

  useEffect(() => {
    dispatch(skillsList());
  }, []);

  useEffect(() => {
    console.log("skillsLis :", skillsLis);
    let data = { skills: skillsLis.skills };
    setSkills(data);
  }, [skillsLis]);

  const addSkill = () => {
    if (inputValue.trim() !== "") {
      const updatedSkills = [...skill.skills, inputValue];
      setSkills({ ...skill, skills: updatedSkills });
      setInputValue("");
      dispatch(skillsUpdate(updatedSkills));
    }
  };

  const RemoveValue = (index) => {
    const updatedSkills = [...skill.skills];
    updatedSkills.splice(index, 1);
    setSkills({ ...skill, skills: updatedSkills });
    dispatch(skillsUpdate(updatedSkills));
  };

  function createSkill() {
    setCreate(true);
  }

  return (
    <>
      <Layout header={"My Profile"}>
        <div className="profile-section">
          {skill?.skills?.length > 0 || create ? (
            <>
              <div className="tabel-header">
                <SubTitele>{"Skills"}</SubTitele>
                <div className="form-section">
                  <input
                    type="text"
                    value={inputValue}
                    onChange={handleChange}
                  />
                </div>
                <Button functions={addSkill}>+ Add Skills</Button>
              </div>
              <SubTitele>Your Skills {skill.skills.length}</SubTitele>
              <div className="skils-section">
                {skill?.skills?.map((item, index) => (
                  <p key={index}>
                    {item}{" "}
                    <span onClick={() => RemoveValue(index)}>&times;</span>
                  </p>
                ))}
              </div>
            </>
          ) : (
            <ProfileLayout
              content={"No Educational records found"}
              img={EducationIcon}
              buttonContent={"+ Add Skills"}
              openPopup={createSkill}
            />
          )}
        </div>
      </Layout>
    </>
  );
};

export default Skills;
