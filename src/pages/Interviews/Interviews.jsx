import React, { useEffect, useState } from "react";
import SideBar from "../../components/side-bar/SideBar";
import { Layout, TabText } from "../../components/Layout/Layout";
import "./Interviews.scss";
import { useDispatch, useSelector } from "react-redux";
import { InterviewCreate, InterviewList } from "../../redux/actions/InterviewsActions";
import { useForm } from "react-hook-form";
import { SubTitele } from "../../components/subTitele/SubTitele";
import ProfileTable from "../../components/ProfileTable/ProfileTable";
import FormInput from "../../components/input-form/FormInput";
import Button from "../../components/button/Button";
import ProfileLayout from "../../components/Profilelayout/ProfileLayout";
import Popup from "../../components/popup/Popup";
import {
  EducationIcon,
  EmptyStar,
  StarIcon,
  closeIcon,
  halfStarIcon,
  levelIcon,
  modeIcon,
  searchIcon,
  techStack_icon,
} from "../../utils/images";

export default function Interviews() {
  const [activeTab, setActiveTab] = useState(false);

  const technologyOption = useSelector(({ courseList }) => courseList.courses);
  // console.log('tt', technologyOption)

  const ActiveTab = (data) => {
    console.log(data);
    setActiveTab(data);
  };

  const [isModalOpen, setIsModalOpen] = useState(false);
  const [createNew, setCreateNew] = useState(false);
  const [editValue, setEditValue] = useState({});
  // console.log(editValue);
  const dispatch = useDispatch();

  const interviewRequest = useSelector(
    ({ interViewList }) => interViewList.interviewRequestList
  );

  console.log('interview request', interviewRequest)

  useEffect(() => {
    dispatch(InterviewList());
  }, []);

  function editFunction(item) {
    setEditValue(item);
    setIsModalOpen(true);
  }

  function deleteItem(item) {
    // dispatch(educationsListDelete(item));
  }

  const form = useForm({
    defaultValues: editValue,
    mode: "all",
  });

  useEffect(() => {
    form.reset(editValue);
  }, [editValue, form]);
  const { register, control, handleSubmit, formState, setValue, watch, reset } = form;
  const { errors } = formState;

  const onSubmit = async (data) => {
    console.log('data of tech', data)
    dispatch(InterviewCreate(data))
    reset({
      date: '', 
      mode: '', 
      technology_id: '',
      level: '',
    });
    // createNew
    //   ? dispatch(educationsListCreate(data))
    //   : dispatch(educationsListEdit(data));
    setIsModalOpen(false);
    setCreateNew(false);
  };

  const newUser = () => {
    setIsModalOpen(true);
    setEditValue({
      date: "",
      mode: "",
      technology_id: "",
      level: "",
    });
    setCreateNew(true);
  };

  function closePopup() {
    setIsModalOpen(false);
    setCreateNew(false);
  }

  const renderStars = (rating) => {
    const stars = [];
    const fullStars = Math.floor(rating);
    const hasHalfStar = rating % 1 !== 0;

    for (let i = 0; i < fullStars; i++) {
      stars.push(<img key={`star-${i}`} src={StarIcon} alt="Full Star" />);
    }

    if (hasHalfStar) {
      stars.push(<img key="half-star" src={halfStarIcon} alt="Half Star" />);
    }

    const remainingStars = 5 - stars.length;
    for (let i = 0; i < remainingStars; i++) {
      stars.push(<img key={`empty-${i}`} src={EmptyStar} alt="Empty Star" />);
    }

    return stars;
  };

  const formatTime = (time) => {
    if (time) {
      const parsedTime = new Date(`1970-01-01T${time}`);
      return parsedTime.toLocaleTimeString("en-US", {
        hour: "2-digit",
        minute: "2-digit",
      });
    }
    return "";
  };
  const mode = [
    { value: "1", label: "Remote" },
    { value: "2", label: "Onsite" },
  ];
  const level =[
    { value: "1", label: "Beginner" },
    { value: "2", label: "Intermediate" },
    { value: "3", label: "Advanced" },
  ]
  return (
    <>
      <Layout header={"Interviews"}>
        <div className="tad_header">
          <TabText
            classActive={true}
            states={activeTab}
            text={"Achieved"}
            activeFunction={() => ActiveTab(true)}
          />
          <TabText
            classActive={false}
            states={activeTab}
            text={"Request Interview"}
            activeFunction={() => ActiveTab(false)}
          />
        </div>
        <>
          {!activeTab ? (
            <>
              {0 == 0 ? (
                <>
                  <div className="profile-section">
                    <div className="scheduled_interview_header">
                      {" "}
                      <SubTitele>Scheduled interviews</SubTitele>
                      <Button functions={() => setIsModalOpen(true)}>
                        Schedule Interview
                      </Button>
                    </div>

                    <div className="scheduled_interview_card">
                      {interviewRequest &&
                        interviewRequest?.map((item) => (
                          <div className="scheduled_interview_card_section">
                            <h3>
                              {item.technology}
                              <span>{item.level}</span>
                            </h3>
                            <ul>
                              <li>
                                Scheduled On {item.date}{" "}
                                {item.time ? ` / ${formatTime(item.time)}` : ""}
                              </li>
                              <li>{item.mode}</li>
                              <li>{item.status}</li>
                            </ul>
                            <div className="button_section">
                              <Button fill>Cancel</Button>
                              <Button
                                fill
                                disabledButton={item.joiningLink === ""}>
                                Join
                              </Button>
                            </div>
                          </div>
                        ))}

                      {/* <div className="scheduled_interview_card_section">
                        <h3>
                          Asp Dot Net<span>Intermediate</span>
                        </h3>
                        <ul>
                          <li>Scheduled On 23/03/23</li>
                          <li>Remote</li>
                        </ul>
                        <div className="button_section">
                          <Button fill>Cancel</Button>
                          <Button fill>Join</Button>
                        </div>
                      </div> */}
                    </div>
                  </div>
                </>
              ) : (
                <div className="profile-section time_table">
                  <ProfileLayout
                    content={"No Interviews scheduled"}
                    img={EducationIcon}
                    buttonContent={"+ Add Time sheet"}
                    // openPopup={newUser}
                  />
                </div>
              )}
            </>
          ) : (
            <>
              {0 == 0 ? (
                <>
                  <h4>Mock Interviews</h4>
                  <div className="profile-section">
                    <div className="interviews_section">
                      <div className="interviews">
                        <div className="interviews_status">
                          <div className="interviews_status_img">
                            <img src={techStack_icon} alt="" />
                          </div>
                          <div className="interviews_status_text">
                            <h5>Asp Dot Net</h5>
                            <p>Tech Stack</p>
                          </div>
                        </div>
                        <div className="interviews_status">
                          <div className="interviews_status_img">
                            <img src={levelIcon} alt="" />
                          </div>
                          <div className="interviews_status_text">
                            <h5>Beginner</h5>
                            <p>Level</p>
                          </div>
                        </div>
                        <div className="interviews_status">
                          <div className="interviews_status_img">
                            <img src={modeIcon} alt="" />
                          </div>
                          <div className="interviews_status_text">
                            <h5>On site</h5>
                            <p>Mode</p>
                          </div>
                        </div>
                      </div>
                      <div className="interviews_status profile_part">
                        <div className="interviews_status_img">
                          <img src={""} alt="" />
                        </div>
                        <div className="interviews_status_text">
                          <h5>Rio Johnson</h5>
                          <p> {renderStars(3.5)}</p>
                        </div>
                      </div>
                    </div>
                    <div className="Interview_section">
                      <h5>Date of Interview :</h5>
                      <p>24/11/2023</p>
                      <h5>Date of Interview :</h5>
                      <p>
                        Lorem ipsum dolor sit amet, consectetur adipiscing elit,
                        sed do eiusmod tempor incididunt ut labore et dolorepo
                        magnazxvs aliqua.ajioaipqisp Scelerisque fermentum dui
                        faucibus in ornare quam viverra orci. Nulla facilisi
                        morbi tempussoiss iaculis urna idaaaa volutpat lacus
                        laoreeth.xl Donec ac odio tempor orci dapibus ultrices
                        in iaculis. Lorem ipsum dolor sit amet, consectetur
                        adipiscing elit, sed do eiusmod tempor incididunt ut
                        labore et dolorepo magnazxvs aliqua.ajioaipqisp
                        Scelerisque fermentum.
                      </p>

                      <Button>View questions</Button>
                    </div>
                  </div>
                </>
              ) : (
                <div className="profile-section time_table">
                  <ProfileLayout
                    content={"No interviews achieved"}
                    img={EducationIcon}
                    buttonContent={"+ Add Time sheet"}
                    // openPopup={newUser}
                  />
                </div>
              )}
            </>
          )}

          <Popup
            isOpen={isModalOpen}
            onRequestClose={() => setIsModalOpen(false)}>
          

            <div className="popup_header">
              <h3>Schedule Interview</h3>
              <img onClick={closePopup} src={closeIcon} alt="" />
            </div>

            <form onSubmit={handleSubmit(onSubmit)}>
              <div className="form_section add">
                <FormInput
                  label={"Date"}
                  // placeholder={"Enter College Name"}
                  type={"date"}
                  errorMSG={errors.date?.message}
                  required={{
                    ...register("date", {
                      required: "date  is required",
                    }),
                  }}
                  // value={watch("college_name")}
                />
{/* 
                <div className="form_field">
                  <FormInput
                    radioLabel={"Mode"}
                    label={"Remote"}
                    value={1}
                    // placeholder={"Enter Degree"}
                    type={"radio"}
                    errorMSG={errors.mode?.message}
                    required={{
                      ...register("mode", {
                        required: "mode is required",
                      }),
                    }}
                  />
                  <FormInput
                    label={"Onsite"}
                    value={2}
                    // placeholder={"Enter Degree"}
                    type={"radio"}
                    errorMSG={errors.mode?.message}
                    required={{
                      ...register("mode", {
                        required: "mode is required",
                      }),
                    }}
                  />
                </div> */}


<FormInput
              radioLabel={"Mode"}
              type={"radio"}
              options={mode}
              required={{
                ...register("mode"),
              }}
            />
                <FormInput
                  label={"Tech Stack"}
                  select={true}
                  options={technologyOption}
                  errorMSG={errors.technology_id?.message}
                  clipboard={true}
                  required={{
                    ...register("technology_id", {
                      required: "Technology is required",
                    }),
                  }}
                />

                {/* <div className="form_field">
                  <FormInput
                    radioLabel={"Level"}
                    label={"Beginner"}
                    value={1}
                    // placeholder={"Enter Degree"}
                    type={"radio"}
                    errorMSG={errors.level?.message}
                    required={{
                      ...register("level", {
                        required: "level is required",
                      }),
                    }}
                  />
                  <FormInput
                    label={"Intermediate"}
                    value={2}
                    // placeholder={"Enter Degree"}
                    type={"radio"}
                    errorMSG={errors.level?.message}
                    required={{
                      ...register("level", {
                        required: "level is required",
                      }),
                    }}
                  />
                  <FormInput
                    label={"Advanced"}
                    value={3}
                    // placeholder={"Enter Degree"}
                    type={"radio"}
                    errorMSG={errors.level?.message}
                    required={{
                      ...register("level", {
                        required: "level is required",
                      }),
                    }}
                  />
                </div> */}

<FormInput
              radioLabel={"Level"}
              type={"radio"}
              options={level}
              required={{
                ...register("level"),
              }}
            />
              </div>

              <div className=" button_group">
                <Button type="button" functions={closePopup}>
                  Cancel
                </Button>
                <Button type="submit" functions={() => setIsModalOpen(true)}>
                  Schedule
                </Button>
              </div>
            </form>
          </Popup>
        </>
      </Layout>
    </>
  );
}
