import React from "react";
import { Controller } from "react-hook-form";

const FileUploads = ({ label, name, control, setValue, disabledInput }) => {
  return (
    <div key={name} className="form-section">
      <label>{label}</label>
      <Controller
        name={name}
        control={control}
        defaultValue={null}
        render={({ field }) => (
          <input
            className="fileInput"
            type="file"
            accept=".pdf"
            onChange={(e) => {
              const file = e.target.files[0];
              field.onChange(file);
              setValue(name, file);
            }}
            disabled={disabledInput}
          />
        )}
      />
    </div>
  );
};

export default FileUploads;
