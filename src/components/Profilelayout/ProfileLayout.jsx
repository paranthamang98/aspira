import React from 'react'
import Button from '../button/Button'
import "./ProfileLayout.scss"

export default function ProfileLayout({openPopup ,content , img , buttonContent}) {
  return (
    <div className='emty-Layout'>
        <img src={img} alt="bg-image" />
        <p className='emty-Layout-content'>{content}</p>
        <Button functions={openPopup}>{buttonContent}</Button>
    </div>
  )
}
