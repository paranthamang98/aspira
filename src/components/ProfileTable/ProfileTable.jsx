import React, { useState } from "react";
import Button from "../button/Button";
import { SubTitele } from "../subTitele/SubTitele";
import "./ProfileTable.scss";
import Pagnation from "../pagnation/pagnation";
import { DeleteIcon, EditBIcon, OpenEye, pdfIcon } from "../../utils/images";
import Popup from "../popup/Popup";

export default function ProfileTable({
  tabelHeaders,
  values,
  subTitele,
  openPopup,
  openEditPopup,
  deleteItem,
  viewItem,
}) {
  console.log(values, "in table");
  const dataWithHours = values?.map((item) => {
    const startTime = new Date(`1970-01-01T${item.start}`);
    const endTime = new Date(`1970-01-01T${item.end}`);
    const hoursDifference = (startTime - endTime) / (1000 * 60 * 60);

    return {
      ...item,
      hours: hoursDifference.toFixed(2),
      days:
        item.type === "Productivity"
          ? `${values.find((v) => v.type === "Productivity")?.value} %`
          : item.days,
    };
  });

  return (
    <>
      <div className="tabel-header">
        <SubTitele>{subTitele}</SubTitele>

        <Button functions={openPopup}>+ Add {subTitele}</Button>
      </div>
      <div className="table-responsive">
        <table>
          <thead>
            <tr>
              {tabelHeaders?.map((header, index) => {
                return <th key={index}>{header.tabelHeader}</th>;
              })}
            </tr>
          </thead>
          <tbody>
            {dataWithHours?.map((item, index) => (
              <tr key={index}>
                {tabelHeaders.map((header, indexs) => (
                  <td key={indexs}>
                    {header.datavalue === "certificates_url" ||
                    header.datavalue === "actions" ||
                    header.datavalue === "#" ? (
                      <>
                        {header.datavalue === "certificates_url" ? (
                          <>
                            {item[header.datavalue] !== "" ? (
                              <a
                                href={item[header.datavalue]}
                                target="_blank"
                                rel="noopener noreferrer"
                              >
                                <img src={pdfIcon} alt="" />
                              </a>
                            ) : (
                              "-"
                            )}
                          </>
                        ) : header.datavalue === "actions" ? (
                          <>
                            {subTitele == "Experience" && (
                              <span onClick={() => viewItem(item)}>
                                {" "}
                                <img src={OpenEye} alt="" />
                              </span>
                            )}{" "}
                            <span onClick={() => openEditPopup(item)}>
                              {" "}
                              <img src={EditBIcon} alt="" />
                            </span>
                            <span onClick={() => deleteItem(item.id)}>
                              {" "}
                              <img src={DeleteIcon} alt="" />
                            </span>
                          </>
                        ) : (
                          index + 1
                        )}
                      </>
                    ) : item[header.datavalue] === null ? (
                      "-"
                    ) : header.datavalue === "hours" ? (
                      Math.abs(item[header.datavalue]).toFixed(2)
                    ) : (
                      item[header.datavalue]
                    )}
                  </td>
                ))}
              </tr>
            ))}
          </tbody>
        </table>
      </div>
      {/* <Pagnation itemsPerPage={4} /> */}
    </>
  );
}
