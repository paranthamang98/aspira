import { ActionTypes } from "../constants/action-types";

const initialState = {
    languages: [],
  loading: false,
  error: null,
};

export const languagesReducer = (
  state = initialState,
  { type, payload, error }
) => {
  switch (type) {
    case ActionTypes.LANGUAGES_PENDING:
      return {
        ...state,
        loading: true,
        error: null,
      };

    case ActionTypes.LANGUAGES_DATA:
      return {
        ...state,
        loading: false,
        languages: payload,
      };

    case ActionTypes.LANGUAGES_ERROR:
      return {
        ...state,
        loading: false,
        error: error,
        languages: [],
      };

    default:
      return state;
  }
};
