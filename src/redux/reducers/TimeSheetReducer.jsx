import { ActionTypes } from "../constants/action-types";

const initialState = {
  timeSheetList: [],
  loading: false,
  error: null,
};

console.log('in reducer', initialState.timeSheetList)
export const timeSheetListReducer = (
  state = initialState,
  { type, payload, error }
) => {
  switch (type) {
    case ActionTypes.TIMESHEET_PENDING:
      return {
        ...state,
        loading: true,
        error: null,
      };

    case ActionTypes.TIMESHEET_DATA:
      return {
        ...state,
        loading: false,
        timeSheetList: payload,
      };

    case ActionTypes.TIMESHEET_ERROR:
      return {
        ...state,
        loading: false,
        error: error,
        timeSheetList: [],
      };

    default:
      return state;
  }
};
