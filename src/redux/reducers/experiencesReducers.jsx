import { ActionTypes } from "../constants/action-types";


const initialState={
    experiences:[],
    loading:false,
    error:null
    
};


export const    experiencesReducer = (state = initialState, {type, payload,error}) =>{
    switch (type) {
        case ActionTypes.EXPERIENCES_PENDING:
            return {
                ...state, 
                loading:true,
                error:null
            };

            case ActionTypes.EXPERIENCES_DATA:
                return {
                    ...state, 
                    loading:false,
                    experiences:payload
                };
                
            case ActionTypes.EXPERIENCES_PENDING:
                return {
                    ...state, 
                    loading:false,
                    error:error,
                    experiences:[]
                };
        
        default:
            return state;
    }   

}; 



