import apiURL from "../../apis/apiURL";
import { ActionTypes } from "../constants/action-types";
import { loader } from "./loginAuthActions";
import {stageStatusUpdate} from './courseAction'

export const projectsDetailList = () => async (dispatch) => {
  loader(true);
  try {
    const response = await apiURL.get("/submited-project");
    const responseData = response.data;
    if (responseData.status) {
      // console.log("in ac", responseData.data);

      dispatch({
        type: ActionTypes.PROJECT_DETAIL_DATA,
        payload: responseData.data,
      });
    } else {
      // Handle unsuccessful profile fetch
    }
    loader(false);
  } catch (error) {
    console.error("Error in projectdetails:", error);
    loader(false);
  }
};

export const ProjectListCreate = (data, projectStatus) => async (dispatch) => {
  dispatch(loader(true));
  console.log(data, "data", 'project-status', projectStatus);
  try {
    const response = await apiURL.post(`/submited-project`, data, {
      headers: {
        "Content-Type": "multipart/form-data",
      },
    });
    const responseData = response.data;
    if (responseData.status) {
      console.log("in action", responseData);
  
      dispatch(stageStatusUpdate(projectStatus))
      // dispatch(projectsDetailList());
      //   notify("success", "Education has been Updated Successfully");
    } else {
      //   notify("error", "Unsuccessful");
      console.log(responseData, "Unsuccessful");
    }
    dispatch(loader(false));
  } catch (error) {
    dispatch(loader(false));
    // notify("error", "Unsuccessful");
    console.log(error, "error");
  }
};
