import apiURL from "../../apis/apiURL";
import { notify } from "../../components/Layout/Layout";
import { ActionTypes } from '../constants/action-types';
import { loader } from "./loginAuthActions";

export const languagesList = () => async (dispatch) => {
  loader(true)
    try {
      const response = await apiURL.get("/languages");
      const responseData = response.data;
      if (responseData.status) {
        dispatch({ type: ActionTypes.LANGUAGES_DATA, payload: responseData.data });
      } else {
        // Handle unsuccessful profile fetch
      }
      loader(false)
    } catch (error) {
      console.error('Error in certificates:', error);
      loader(false)
    }
  };


  export const languagesListEdit = (data) => async (dispatch) => {
    const id = data.id;
  
  
    loader(true);
    try {
      const response = await apiURL.post(`/languages/${id}`, data, {
        headers: {
          "Content-Type": "multipart/form-data",
        },
      });
       const responseData = response.data;
      if (responseData.status) {
        console.log(responseData.status);
        dispatch(languagesList());
        notify("success", "languages has been Updated Successfully");
      } else {
        notify("error", "Unsuccessful");
        dispatch({ type: ActionTypes.LANGUAGES_ERROR });
      }
      loader(false);
    } catch (error) {
      console.error("Error in languages:", error);
      loader(false);
      notify("error", "Unsuccessful");
    }
  };
  
  export const languagesListDelete = (data) => async (dispatch) => {
    loader(true);
  
    try {
      const response = await apiURL.delete(`/languages/${data}`);
      const responseData = response.data;
      if (responseData.status) {
        dispatch(languagesList());
        notify("error", "languages has been deleted");
      } else {
        notify("error", "Unsuccessful");
      }
      loader(false);
    } catch (error) {
      notify("error", "Unsuccessful");
      loader(false);
    }
  };
  
  export const languagesListCreate = (data) => async (dispatch) => {
    loader(true);
  
    console.log(data, "data");
  
    try {
      const response = await apiURL.post(`/languages`, data, {
        headers: {
          "Content-Type": "multipart/form-data",
        },
      });
       const responseData = response.data;
      if (responseData.status) {
        console.log(responseData.status);
        dispatch(languagesList());
        notify("success", "languages has been Updated Successfully");
      } else {
        notify("error", "Unsuccessful");
        console.log(responseData , "Unsuccessful");
      }
      loader(false);
    } catch (error) {
      loader(false);
      notify("error", "Unsuccessful");
      console.log(error, "error");
    }
  };