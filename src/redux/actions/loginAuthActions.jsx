
import apiURL from "../../apis/apiURL";
import { ActionTypes } from '../constants/action-types';
import { notify } from "../../components/Layout/Layout";

export const UseLogin  = (data, onSuccessCallback) => async (dispatch) => {
  loader(true)
  try {
    const response = await apiURL.post("/login", data);
    const responseData = response.data;

    console.log(responseData, "api");

    if (responseData.status) {
  

      notify("success","successful login")
      dispatch({ type: ActionTypes.LOGIN_DATA, payload: responseData });
   

      // Store user token in session storage
      localStorage.setItem('token', responseData.token);
      localStorage.setItem('user', JSON.stringify(responseData));
      onSuccessCallback();
    } else {
      // Handle unsuccessful login
      // localStorage.removeItem('token');
      localStorage.removeItem('user');
    }
    loader(false)
  } catch (error) {
  loader(false)

    console.error('Error in useLogin:', error);
    localStorage.removeItem('token');
    // localStorage.removeItem('user');
  }
};







export const forgetPassword = (url,data ) => async (dispatch) => {
  dispatch(loader(true))
  console.log(data ,"forgetPassword");
    try {
      const response = await apiURL.post(url, data);
      let responseData = response.data;
  
      console.log(responseData, "apiefffeffe");
      
      if (responseData) {
        console.log(responseData);
        notify("success",responseData.message)
        // localStorage.removeItem('token');
        // Call the onSuccessCallback function to trigger navigation
        // onSuccessCallback();
      }
      dispatch({ type: ActionTypes.LOADER, payload:false });
    } catch (error) {
      console.error(error);
      notify("error",error.response.data.message)
      notify("error",error.response.data.errors.token[0])
      dispatch({ type: ActionTypes.LOADER, payload:false });
      console.log(error.response.data.errors.token );
    //   localStorage.removeItem('token');
    }
  };


  export const logOut = () => async () => {
    alert("ssss")
    try {
      const response = await apiURL.post("/logout");
      let responseData = response.data;
  
      console.log(responseData, "apiefffeffe");
  
      if (responseData.status) {
        console.log(responseData , "logoutScreen");
        localStorage.removeItem('token');
      }
    } catch (error) {
      console.error(error,"logoutScreen");
      localStorage.removeItem('token');
    }
  };




  export const Register  = (data) => async (dispatch) => {
    console.log(data,"dtaaaaaaaa");
   const datas = {
    firstName:data.firstName,
lastName:data.lastName,
email:data.email,
mobile_no:data.mobile_no,
gender:data.gender,
dob:data.dob,
join_date:data.join_date,
mode:data.mode,
availability:data.availability,
preferd_techId:data.preferd_techId,
working_status:data.working_status,
laptop_status:data.laptop_status,
resume: data.resume,
parent_name:data.parent_name,
parent_mobile_no:data.parent_mobile_no,
linkedIn_id:data.linkedIn_id,
graduation_details:data.graduation_details
   }
   console.log(datas,"update data");
    try {
      const response = await apiURL.post("/register", data, {
        headers: {
          "Content-Type": "multipart/form-data",
        },
      });
      const responseData = response.data;
  
      console.log(responseData, "Register");
  
      if (responseData.status) {
    
  console.log(responseData ,"Register inside");
        notify("success","successful login")
        // dispatch({ type: ActionTypes.LOGIN_DATA, payload: responseData });
     
  
        // Store user token in session storage
        // localStorage.setItem('token', responseData.token);
        // localStorage.setItem('user', JSON.stringify(responseData));
        // onSuccessCallback();
      } else {
        // Handle unsuccessful login
        // localStorage.removeItem('token');
        // localStorage.removeItem('user');
      }
      // loader(false)
    } catch (error) {
    // loader(false)
  
      console.error('Error in useLogin:', error);
    //   localStorage.removeItem('token');
    //   // localStorage.removeItem('user');
    }
  };




  export const TechnologiesList  = () => async (dispatch) => {

    try {
      const response = await apiURL.get("/technologies");
      const responseData = response.data;
  
  
      if (responseData.status) {
        dispatch({ type: ActionTypes.TECHNOLOGIES_LIST, payload: responseData });
  console.log(responseData ,"TechnologiesList inside");
      } else {

      }
      // loader(false)
    } catch (error) {
      console.error('Error in useLogin:', error);
    }
  };

export const loader=(data)=>{
return (dispatch)=>{
  dispatch({ type: ActionTypes.LOADER, payload:data });
}
}

