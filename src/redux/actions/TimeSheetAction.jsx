import apiURL from "../../apis/apiURL";
import { notify } from "../../components/Layout/Layout";
import { ActionTypes } from "../constants/action-types";
import { loader } from "./loginAuthActions";

export const TimeSheetList = () => async (dispatch) => {
  loader(true);
  try {
    const response = await apiURL.get("/timesheet");
    const responseData = response.data;
    if (responseData.status) {
      dispatch({
        type: ActionTypes.TIMESHEET_DATA,
        payload: responseData.data,
      });
    //   notify("successful", "Education has been deleted");
    } else {
      // Handle unsuccessful profile fetch
    }
    loader(false);
  } catch (error) {
    console.error("Error in projectdetails:", error);
    loader(false);
  }
};

export const TimeSheetListCreate = (data) => async (dispatch) => {
  dispatch(loader(true));
  console.log(data, "data");
  try {
    const response = await apiURL.post(`/timesheet`, data, {
      headers: {
        "Content-Type": "multipart/form-data",
      },
    });
    const responseData = response.data;
    if (responseData.status) {
      console.log('in action',responseData);
      dispatch(TimeSheetList());
    //   notify("success", "Education has been Updated Successfully");
    } else {
    //   notify("error", "Unsuccessful");
      console.log(responseData, "Unsuccessful");
    }
    dispatch(loader(false));
  } catch (error) {
    dispatch(loader(false));
    // notify("error", "Unsuccessful");
    console.log(error, "error");
  }
};



export const TimeSheetListDelete = (data) => async (dispatch) => {
  loader(true);

  try {
    const response = await apiURL.delete(`/timesheet/${data}`);
    const responseData = response.data;
    if (responseData.status) {
      // dispatch({
      //   type: ActionTypes.TIMESHEET_DATA,
      //   payload: responseData.data,
      // });
      dispatch(TimeSheetList());
      notify("error", "Education has been deleted");
    } else {
      //   notify("error", "Unsuccessful");
    }
    loader(false);
  } catch (error) {
    // notify("error", "Unsuccessful");
    loader(false);
  }
};

