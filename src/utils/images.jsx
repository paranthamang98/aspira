import Logo from "../asset/images/logo.svg";
import DashboardLogo from "../asset/images/dashboard-logo.svg";
import CloseEye from "../asset/images/closei-icon.png";
import OpenEye from "../asset/images/open-i-icon.png";
import dashboardIcon from "../asset/images/dashboard-icon.svg";
import MyLearningsIcon from "../asset/images/My-Learnings-icon.svg";
import MyProgressIcon from "../asset/images/My-Progress-icon.svg";
import MyProjectsIcon from "../asset/images/My-Projects-icon.svg";
import RewardsIcon from "../asset/images/Rewards-icon.svg";
import InterviewsIcon from "../asset/images/Interviews-icon.svg";
import logoutIcon from "../asset/images/logout-icon.svg";
import NotificationIcon from "../asset/images/notification-bing-icon.svg";
import ProfileIcon from "../asset/images/profile-icon.svg";
import TimeBasedGreetingimg from "../asset/images/TimeBasedGreeting.svg";
import CoursesIcon from "../asset/images/courses-icon.svg";
import GoalIcon from "../asset/images/goal-icon.svg";
import TimesheetIcon from "../asset/images/timesheetIcon.svg";
import RankingIcon from "../asset/images/rankingIcon.svg";
import RewardIcon from "../asset/images/rewardIcon.svg";
import ArrowRight from "../asset/images/arrowRight.svg";
import InterviewIconAl from "../asset/images/clipboard-interviewIcon.svg";
import HambergerIcon from "../asset/images/hamberger.svg";
import AlertIcon from "../asset/images/alert.svg";
import MenuLogo from "../asset/images/menu-logo.svg";
import EditIcon from "../asset/images/edit-icon.svg";
import EditBIcon from "../asset/images/edit-icon-blu.svg";
import EducationIcon from "../asset/images/education-emty.svg";
import pdfIcon from "../asset/images/pdfIcon.svg";
import DeleteIcon from "../asset/images/deleteIcon.svg";
import PasteIcon from "../asset/images/pasteIcon.svg";
import SliderArrow from "../asset/images/sliderArrow.svg";
import courseLockIcon from "../asset/images/course-lock-icon.png";
import lockFillIcon from "../asset/images/lock-fill-icon.svg";
import noteIcon from "../asset/images/noteIcon.svg";
import uploadImg from "../asset/images/uploadImg.svg";
import searchIcon from "../asset/images/searchIcon.svg";
import StarIcon from "../asset/images/StarIcon.svg";
import halfStarIcon from "../asset/images/halfStarIcon.svg";
import techStack_icon from "../asset/images/techStack_icon.svg";
import levelIcon from "../asset/images/levelIcon.svg";
import modeIcon from "../asset/images/modeIcon.svg";
import EmptyStar from "../asset/images/EmptyStar.svg";
import closeIcon from "../asset/images/close-circle.svg";   
import codeIcon from "../asset/images/code.svg";
import searchProjectIcon from "../asset/images/search-normal.svg";
import shareIcon from "../asset/images/share.svg";
import downloadIcon from "../asset/images/DownladIcon.png";
import clockIcon from "../asset/images/clock.svg";


export { 
    Logo, 
    DashboardLogo,
    CloseEye, 
    OpenEye, 
    dashboardIcon,
    MyLearningsIcon, 
    MyProgressIcon,
    MyProjectsIcon,
    RewardsIcon,
    InterviewsIcon,
    logoutIcon,
    NotificationIcon,
    ProfileIcon,
    TimeBasedGreetingimg,
    CoursesIcon,
    GoalIcon,
    TimesheetIcon,
    RankingIcon,
    RewardIcon,
    ArrowRight,
    InterviewIconAl,
    HambergerIcon,
    AlertIcon,
    MenuLogo,
    EditIcon,
    EducationIcon,
    DeleteIcon,
    pdfIcon,
    PasteIcon,
    EditBIcon,
    SliderArrow,
    courseLockIcon,
    lockFillIcon,
    noteIcon,
    halfStarIcon,
    StarIcon,
    uploadImg ,
    searchIcon,
    techStack_icon,
    levelIcon,
    modeIcon,
    EmptyStar,
    closeIcon,
    // uploadImg ,
    // searchIcon,
    codeIcon,
    downloadIcon,
    shareIcon,
    searchProjectIcon,
    clockIcon
 };